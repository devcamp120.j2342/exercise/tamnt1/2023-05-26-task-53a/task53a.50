
public class App {
    public static void main(String[] args) throws Exception {
        Account account1 = new Account(1, "Tam");
        Account account2 = new Account(2, "Minh", 1000);

        System.out.println(account1.toString() + account2.toString());
        account1.credit(2000);
        account2.credit(3000);

        System.out.println(account1.toString() + account2.toString());
        account1.debit(1000);
        account2.debit(5000);

        System.out.println(account1.toString() + account2.toString());

        account1.tranfer(account2, 1000);
        System.out.println(account1.toString() + account2.toString());
        account2.tranfer(account1, 2000);
        System.out.println(account1.toString() + account2.toString());
    }

}